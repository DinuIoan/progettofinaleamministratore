package com.progettofinale.servlets.ordine;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.progettofinale.dao.OrdiniDao;
import com.progettofinale.dao.ProdottoDao;
import com.progettofinale.model.Ordine;
import com.progettofinale.model.Prodotto;

@WebServlet("/insertordine")
public class InsertOrdine extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public InsertOrdine() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.print("ERRORE: questa tipologia di chiamata non � permessa!");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Integer codiceU = request.getParameter("inputUtente") != null ? Integer.parseInt(request.getParameter("inputUtente")): -1;
        String codiceP = request.getParameter("inputProdotto") != null ? request.getParameter("inputProdotto") : "";
        Integer quantiP = request.getParameter("inputQuantita") != null ? Integer.parseInt(request.getParameter("inputQuantita")) : -1;
        Date dataO = new Date (request.getParameter("inputData"));

        OrdiniDao oDao = new OrdiniDao();
        Ordine ord=new Ordine();
        Prodotto prod =new Prodotto();
        ProdottoDao pdao=new ProdottoDao();

        ord.setCodiceUtente(codiceU);
        ord.setDataOrdine(dataO);


        PrintWriter out=response.getWriter();

        try {

            if(codiceU!=-1 && codiceP!="" && quantiP!=-1 )
            {
            prod=pdao.getByCodice(codiceP);
            prod.setQuantitaDisponibile(quantiP);
            ord.addProdotto(prod);
            oDao.insert(ord);

            }

        }catch(SQLException e){
            out.print("Errore in inserimento: " + e.getMessage());
        }

    }

}
