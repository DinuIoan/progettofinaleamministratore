package com.progettofinale.servlets.ordine;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettofinale.dao.OrdiniDao;
import com.progettofinale.model.Ordine;

@WebServlet("/getordine")
public class GetOrdine extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetOrdine() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Gson jasonizzatore = new Gson();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		OrdiniDao odao = new OrdiniDao();
		try {
			ArrayList<Ordine> elencoTemp = odao.getAll();

			out.print(jasonizzatore.toJson(elencoTemp));

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.print("ERRORE: non puoi usare questo tipo di chiamata!");
	}
}