package com.progettofinale.servlets.utente;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.progettofinale.dao.UtenteDao;
import com.progettofinale.model.Utente;

/**
 * Servlet implementation class VerificaLogin
 */
@WebServlet("/verificalogin")
public class VerificaLogin extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String user = request.getParameter("input_username") != null ? request.getParameter("input_username") : "";
		String pass = request.getParameter("input_password") != null ? request.getParameter("input_password") : "";

		Utente uTemp = new Utente();
		UtenteDao uDao = new UtenteDao();
		try {
			uTemp = uDao.getByUsername(user);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (uTemp.getIdUtente() != null) {

				HttpSession sessione = request.getSession();

				if (uTemp.getTipologiaU().equals("ADMIN")) {
					sessione.setAttribute("role", "ADMIN");
					sessione.setAttribute("username", uTemp.getUsernameU());
					PrintWriter out = response.getWriter();
					out.print("ADMIN");

					return;
				}

				if (uTemp.getTipologiaU().equals("UTENTE")) {
					sessione.setAttribute("role", "UTENTE");
					sessione.setAttribute("username", uTemp.getUsernameU());
					PrintWriter out = response.getWriter();
					out.print("UTENTE");
					return;
				}
				if (uTemp.getTipologiaU().equals("STORE")) {
					sessione.setAttribute("role", "STORE");
					sessione.setAttribute("username", uTemp.getUsernameU());
					PrintWriter out = response.getWriter();
					out.print("STORE");
					return;
				}
				
				
				

			}

		}

	}
}
