package com.progettofinale.servlets.utente;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.progettofinale.dao.UtenteDao;
import com.progettofinale.model.Utente;

/**
 * Servlet implementation class AggiornaUtente
 */
@WebServlet("/aggiornautente")
public class AggiornaUtente extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	Integer varId = Integer.parseInt(request.getParameter("input_id"));
	System.out.println(varId);
	String	varUsername =request.getParameter("input_usernameU");
	String varPassword = request.getParameter("input_paswU");
	String varIndirizzo = request.getParameter("input_indirizzo");
	
	Utente uTemp = new Utente();
	UtenteDao uDao = new UtenteDao();
	try {
		uTemp = uDao.getById(varId);
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	uTemp.setPswU(varPassword);
	uTemp.setUsernameU(varUsername);
	uTemp.setIndirizzo(varIndirizzo);
	

	try {
		uDao.update(uTemp);
		response.sendRedirect("ADMINgestisciUtenti.jsp");
		System.out.println(" utenteaggiornato ");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println(e.getMessage());
	}
	
		
		
		
		
		
	}

}
