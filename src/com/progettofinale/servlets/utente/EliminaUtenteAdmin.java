package com.progettofinale.servlets.utente;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.progettofinale.dao.UtenteDao;
import com.progettofinale.model.Utente;

/**
 * Servlet implementation class EliminaUtenteAdmin
 */
@WebServlet("/eliminautenteadmin")
public class EliminaUtenteAdmin extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		Integer idUtenteDaEliminare = Integer.parseInt(request.getParameter("idUtente"));
		
		UtenteDao uDao = new UtenteDao();
		Utente uTemp = new Utente();
		try {
			uTemp = uDao.getById(idUtenteDaEliminare);
			if(uDao.delete(uTemp)) {
			  System.out.println("utente eliminato");	    
			response.sendRedirect("ADMINgestisciUtenti.jsp");
			}
			else {
				System.out.println("utente non eliminato");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
		

}
