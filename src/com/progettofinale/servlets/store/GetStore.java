package com.progettofinale.servlets.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.progettofinale.dao.StoreDao;
import com.progettofinale.model.Store;

@WebServlet("/getstore")
public class GetStore extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetStore() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		Gson jasonizzatore = new Gson();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		StoreDao sdao = new StoreDao();
		try {
			ArrayList<Store> elencoTemp = sdao.getAll();

			out.print(jasonizzatore.toJson(elencoTemp));

		} catch (SQLException e) {
			out.print(e.getMessage());
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.print("ERRORE: non puoi usare questa tipologia di chiamata!");
	}

}