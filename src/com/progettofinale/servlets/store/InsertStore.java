package com.progettofinale.servlets.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.progettofinale.dao.StoreDao;
import com.progettofinale.model.Store;

@WebServlet("/insertstore")
public class InsertStore extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public InsertStore() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nomeS = request.getParameter("input_nomeS") != null ? request.getParameter("input_nomeS") : "";
		String indirizzo = request.getParameter("input_indirizzo") != null ? request.getParameter("input_indirizzo") : "";
		
		
		PrintWriter out=response.getWriter();
		  
		try {
				if(!nomeS.equals("")) {
					StoreDao stDao = new StoreDao();
					Store st=new Store();
					st.setNomeS(nomeS);
					st.setIndirizzo(indirizzo);
					stDao.insert(st);
					
					if(st.getIdStore()>0) {
						   out.print(true); 
						  }
						  else {
						   out.print(false);
						  }
				}
				else
					out.print("Errore nell'inserimento dei campi!");

		}catch(SQLException e){
			System.out.println("Errore in inserimento messaggio: " + e.getMessage());
		}

	}

}