package com.progettofinale.servlets.store;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.progettofinale.dao.StoreDao;
import com.progettofinale.model.Store;

@WebServlet("/updatestore")
public class UpdateStore extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UpdateStore() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String nomeS = request.getParameter("inputNome");
		String indirizzo = request.getParameter("inputIndirizzo");
	
		Store temp = new Store();
		temp.setNomeS(nomeS);
		temp.setIndirizzo(indirizzo);
	
		StoreDao sdao = new StoreDao();
		PrintWriter out = response.getWriter();
	
		try {
			Store tempAgg = sdao.update(temp);
			if ((Integer) tempAgg.getIdStore() != null) {
				out.print(true);
			} else {
				out.print(false);
			}
		} catch (SQLException e) {
			out.print(e.getMessage());
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.write("ERRORE: non puoi usare questo tipo di chiamata!");
	}

}