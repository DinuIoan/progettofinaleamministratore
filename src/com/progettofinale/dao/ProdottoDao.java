package com.progettofinale.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettofinale.connessione.Connector;
import com.progettofinale.model.Prodotto;
import com.progettofinale.model.Utente;

public class ProdottoDao implements Dao<Prodotto> {

	@Override
	public Prodotto getById(Integer id) throws SQLException {

		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idProdotto, codiceP, nomeP, prezzoP FROM Prodotto WHERE idProdotto = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet result = ps.executeQuery();
		result.next();

		Prodotto temp = new Prodotto();

		temp.setIdProdotto(result.getInt(1));
		temp.setCodiceP(result.getString(2));
		temp.setNomeP(result.getString(3));
		temp.setPrezzo(result.getDouble(4));

		return temp;
	}
	
	public Prodotto getByCodice(String codice) throws SQLException{
		
		Connection conn = Connector.getIstance().getConnection();
		String query="SELECT SELECT idProdotto, codiceP, nomeP, prezzoP FROM Prodotto WHERE codiceP = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, codice);
		ResultSet result = ps.executeQuery();
		result.next();
		
		Prodotto temp = new Prodotto();
		
		temp.setIdProdotto(result.getInt(1));
		temp.setCodiceP(result.getString(2));
		temp.setNomeP(result.getString(3));
		temp.setPrezzo(result.getDouble(4));
		
		return temp;
	}

	@Override
	public ArrayList<Prodotto> getAll() throws SQLException {

		ArrayList<Prodotto> elencoProdotti = new ArrayList<Prodotto>();
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idProdotto, codiceP, nomeP, prezzoP FROM Prodotto";
		PreparedStatement ps = conn.prepareStatement(query);

		ResultSet result = ps.executeQuery();
		while (result.next()) {
			Prodotto temp = new Prodotto();
			temp.setIdProdotto(result.getInt(1));
			temp.setCodiceP(result.getString(2));
			temp.setNomeP(result.getString(3));
			temp.setPrezzo(result.getDouble(4));

			elencoProdotti.add(temp);
		}

		return elencoProdotti;
	}

	@Override
	public void insert(Prodotto t) throws SQLException {
		Connection conn = Connector.getIstance().getConnection();
		
		String query="INSERT INTO Prodotto (codiceP, nomeP, prezzoP) VALUES (?,?,?)";
		PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getCodiceP());
		ps.setString(2, t.getNomeP());
		ps.setDouble(3, t.getPrezzo());
		
		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		
		t.setIdProdotto(result.getInt(1));

	}

	@Override
	public boolean delete(Prodotto t) throws SQLException {
		
		Connection conn = Connector.getIstance().getConnection();
		
		int id = this.getByCodice(t.getCodiceP()).getIdProdotto();
		
		String query="DELETE FROM Prodotto WHERE idProdotto = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		
		int deletedRows = ps.executeUpdate();
		
		if(deletedRows > 0 )
			return true;
		return false;
	}

	@Override
	public Prodotto update(Prodotto t) throws SQLException {
		
		Connection conn = Connector.getIstance().getConnection();
		String query = "UPDATE Prodotto SET nomeP = ?, prezzoP = ? WHERE idProdotto = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, t.getNomeP());
		ps.setDouble(2, t.getPrezzo());
		ps.setInt(3, t.getIdProdotto());
		
		int updatedRows = ps.executeUpdate();
		
		if(updatedRows > 0)
			return t;
		return null;
	}

}