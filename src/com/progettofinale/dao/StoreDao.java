package com.progettofinale.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettofinale.connessione.Connector;
import com.progettofinale.model.Prodotto;
import com.progettofinale.model.Store;

public class StoreDao implements Dao<Store> {
	
	/**
	 * Funzione che mi restituisce un oggetto di tipo Store ricercandolo nel DB in base al suo id passato come
	 * parametro.
	 */
	@Override
	public Store getById(Integer id) throws SQLException {
		Store st = null;
		Prodotto pr = null;
		ArrayList<Prodotto> elp=new ArrayList<Prodotto>();
		Connection conn = Connector.getIstance().getConnection();
		String query = "SELECT idStore, nomeS,indirizzo, idProdottoPS,idStorePS, quantitaDisponibile, idProdotto, codiceP, nomeP, prezzoP FROM Store JOIN ProdottoStore ON Store.idStore = ProdottoStore.idStorePS"
				+ "    JOIN Prodotto ON ProdottoStore.idProdottoPS = Prodotto.idProdotto WHERE idStore=?;";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet result = ps.executeQuery();
		while (result.next()) {
			st = new Store();
			pr = new Prodotto();
			st.setIdStore(result.getInt(1));
			st.setNomeS(result.getString(2));
			st.setIndirizzo(result.getString(3));
			pr.setIdProdotto(result.getInt(4));
			pr.setQuantitaDisponibile(result.getInt(6));
			pr.setNomeP(result.getString(9));
			pr.setCodiceP(result.getString(8));
			pr.setPrezzo(result.getDouble(10));
			elp.add(pr);
			
		}
		st.setElencoProdotti(elp);
		return st;

	}
	
	/**
	 * Funzione che mi restituisce un ArrayList di tutti gli store
	 */
	@Override
	public ArrayList<Store> getAll() throws SQLException {
		 ArrayList<Store> elenco = new ArrayList<Store>();

		    Connection conn = Connector.getIstance().getConnection();
		    String query = "SELECT idStore, nomeS,indirizzo, idProdottoPS,idStorePS, quantitaDisponibile, idProdotto, codiceP, nomeP, prezzoP FROM Store JOIN ProdottoStore ON Store.idStore = ProdottoStore.idStorePS  "
		            + "    JOIN Prodotto ON ProdottoStore.idProdottoPS = Prodotto.idProdotto;";
		    PreparedStatement ps = conn.prepareStatement(query);
		    ResultSet result = ps.executeQuery();
		    while (result.next()) {
		        Store st = new Store();
		        Prodotto pr = new Prodotto();
		        st.setIdStore(result.getInt(1));
		        st.setNomeS(result.getString(2));
		        st.setIndirizzo(result.getString(3));
		        pr.setIdProdotto(result.getInt(4));
		        pr.setQuantitaDisponibile(result.getInt(6));
		        pr.setNomeP(result.getString(9));
		        pr.setCodiceP(result.getString(8));
		        pr.setPrezzo(result.getDouble(10));

		        if (elenco.size() > 0) {
		            if (!elenco.contains(st)) {
		                elenco.add(st);
		                if (!st.getElencoProdotti().contains(pr)) {
		                    st.addProdotto(pr);
		                }

		            }

		        } else {
		            st.addProdotto(pr);
		            elenco.add(st);
		        }
		    }

		    return elenco;
	}
	
	/**
	 * Funzione che inserisce uno Store, passato come parametro, nel DB
	 */
	@Override
	public void insert(Store t) throws SQLException {
	
		Connection conn = Connector.getIstance().getConnection();
		String query="INSERT INTO Store (nomeS, indirizzo) VALUES (?,?)";
		PreparedStatement ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, t.getNomeS());
		ps.setString(2, t.getIndirizzo());
		
		ps.executeUpdate();
		ResultSet result = ps.getGeneratedKeys();
		result.next();
		t.setIdStore(result.getInt(1));

	}
	
	/**
	 * Funzione che ricevuto uno Store in parametro prova a eliminarlo nel DB. Se va a buon fine ritorna true
	 * altrimenti false.
	 */
	@Override
	public boolean delete(Store t) throws SQLException {
		
		Connection conn = Connector.getIstance().getConnection();
		int id = t.getIdStore();
		String query="DELETE FROM Store WHERE idStore = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		
		int deletedRows = ps.executeUpdate();
		
		if(deletedRows > 0 )
			return true;
		return false;

	}

	@Override
	public Store update(Store t) throws SQLException {
		
		Connection conn = Connector.getIstance().getConnection();
		String query = "UPDATE Store SET nomeS = ?, indirizzo = ? WHERE idStore = ?";
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, t.getNomeS());
		ps.setString(2, t.getIndirizzo());
		ps.setInt(3, t.getIdStore());
		
		int updatedRows = ps.executeUpdate();
		
		if(updatedRows > 0)
			return t;
		return null;
	}
	
	public ArrayList<Store> restituiscimiTuttiLiStore() throws SQLException {

		ArrayList<Store> elencoStore = new ArrayList<Store>();
		Connection conn = Connector.getIstance().getConnection();
		String query = " Select idStore,nomeS,indirizzo from Store;";
		PreparedStatement ps = conn.prepareStatement(query);

		ResultSet result = ps.executeQuery();
		while (result.next()) {

			Store sTemp = new Store();
			sTemp.setIdStore(result.getInt(1));
			sTemp.setNomeS(result.getString(2));
			sTemp.setIndirizzo(result.getString(3));
			elencoStore.add(sTemp);

		}
		return elencoStore;
	}
}