drop database if exists delivery;
create database delivery;
use delivery;

Create table Utente(
idUtente integer not null auto_increment primary key,
usernameU varchar (250) unique not null,
paswU varchar(250) not null,
tipologiaU varchar (250) not null constraint tipologiaU check (tipologiaU in ("ADMIN", "STORE","UTENTE")),
indirizzo varchar(250) not null);

insert into Utente(usernameU,paswU,tipologiaU,indirizzo) values
("Giovanni","1234","ADMIN","via Roma"),
("Mario","1234","STORE"," Via MAdama Cristina"),
("Valeria","1234","UTENTE","Via G.Gentile"),
("Ciccio","1234","UTENTE", "Via Nessuna");

Create table Prodotto(
idProdotto integer not null auto_increment primary key,
codiceP varchar(250) not null unique ,
nomeP varchar(250) not null,
prezzoP double default 0
);

Insert into Prodotto(codiceP,nomeP,prezzoP) values
("PIZ123","Pizza",5.50),
("PANZ123","Panzerotto",3.5),
("PIAD123","Piadina", 5);

Create table UtenteProdotto(
codiceUtente integer not null,
codiceProdotto integer not null,
quantiProdotto integer not null,
prezzoUP double default 0 ,
dataUP date,
foreign key(codiceUtente) references Utente(idUtente) on delete cascade,
foreign key(codiceProdotto) references Prodotto(idProdotto) on delete no action,
primary key(codiceUtente,codiceProdotto)
);

insert into UtenteProdotto(codiceUtente,codiceProdotto,quantiProdotto,dataUP) values
(3,1,3,"2020-01-02"),
(3,2,1,"2020-01-02"),
(4,3,5,"2021-03-17");

select codiceUtente,codiceProdotto,codiceProdotto,dataUP from UtenteProdotto;

Create table Store(
idStore integer not null auto_increment primary key,
nomeS varchar(250) not null unique,
indirizzo varchar(250) not null unique
);

insert into Store(nomeS,indirizzo) values
("Pizza Speedy","Via della pizza"),
("Horas Kebab","Corso V.Ema");

Create table ProdottoStore (
idProdottoPS integer not null,
idStorePS integer not null,
quantitaDisponibile integer not null default 0,
foreign key(idProdottoPS) references Prodotto(idProdotto) on delete cascade,
foreign key(idStorePS) references Store(idStore) on delete cascade,
primary key(idProdottoPS,idStorePS)
);

insert into ProdottoStore(idProdottoPS,idStorePS,quantitaDisponibile) values
(1,1,150),
(2,2,250),
(3,2,250);

select * from ProdottoStore;
select* from UtenteProdotto;

-- delete from UtenteProdotto where codiceUtente=3;
SELECT * FROM Utente
JOIN UtenteProdotto ON Utente.idUtente = UtenteProdotto.codiceUtente
JOIN Prodotto ON UtenteProdotto.codiceProdotto = Prodotto.idProdotto where idUtente=3;