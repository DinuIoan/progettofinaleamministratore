<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
    <!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Log In</title>
</head>
<body>
    
    
    

      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">HOME</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item active">
              <a class="nav-link" href="ADMINgestisciUtenti.jsp">Gestisci Utenti <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="ADMINgestisciStore.jsp">Gestisci Store</a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="ADMINgestisciOrdini.jsp">Gestisci Ordini</a>
            </li>
          </ul>
        </div>
      </nav>
    

    <div class="container">

        <div class="row mt-5">
            <div class="col">
                <h1>Caratteristiche Prodotto</h1>
                <p>Di seguito puoi consultare tutta la lista dei prodotti nello store</p>
                <p> Per tornare alla pagina di benvenuto premi <a href="index.html">qui </a></p>
            </div>
        </div>
        


<%

HttpSession sessione = request.getSession();
String nomeStore = (String) sessione.getAttribute("nome");
String indirizzoStore = (String) sessione.getAttribute("indirizzo");
String caratteristiche = (String) sessione.getAttribute("listaProdotti");






%>

        
        <div class="row mt-5">
            <div class="col">

                <table class="table">
                    <thead>
                        <tr>
                            <th>Nome </th>
                            <th>indirizzo  </th>
                            
                            <th>caratteristiche  </th>
                          
                        </tr>
                    </thead>
                    <tbody>
                    	   <td><%out.print(nomeStore); %></td>
                            <td><%out.print(indirizzoStore); %> </td>
                            
                            <td> <%out.print(caratteristiche); %></td>
                          
                  
                    
                    </tbody>
                </table>

            </div>
        </div>
        
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
    
    
    
    